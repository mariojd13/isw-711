const express = require('express');
const app = express();
const cors = require("cors");

// check for cors
app.use(cors({
  domains: '*',
  methods: "*"
}));


// listen to GET requests on /hello
app.get('/hello', function (req, res) {
  res.send('World');
});


app.listen(8080, () => console.log(`Example app listening on port 8080!`))
