const express = require('express');
const graphqlHTTP = require('express-graphql');
const uuid = require('uuid/v4');
const mongoose = require("mongoose");
const { graphQLschema } = require('./graphql-schema.js')
const Client = require("../models/clientModel");

// test data (for testing purposes)
// this might be replaced by real database structures

const clientPost = (req, res) => {
  var client = new Client();

  client.id = req.body.id;
  client.name = req.body.name;
  client.lastName = req.body.lastName;

  if (client.id && client.name && client.lastName) {
    client.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the client', err)
        res.json({
          error: 'There was an error saving the client'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:4000/graphql/clients/?id=${client.id}`
      });
      res.json(client);
    });
  } else {
    res.status(422);
    console.log('error while saving the client')
    res.json({
      error: 'No valid data provided for client'
    });
  }
};

let orders = [{
  client: {
    id: uuid(),
    name: "John",
    lastName: "Doe"
  },
  products: [
    {
      id: uuid(),
      name: "Product 1",
      quantity: 2,
      price: 100
    }
  ]
}]

// functions
const getOrder = (request) => {
  return orders[request.orderId];
}
const addClient = (client) => {
  // generate the uid
  const id = uuid();
  // create the client object
  const newClient = {
    id,
    name: client.name,
    lastName: client.lastName
  };
  // add client to the clients array
  clients[id] = newClient;

  // es6-way
  // clients[id] = { ...client, id };

  return clients[id];

}

const clientGet = (req, res) => {
  // if an specific Client is required
  if (req.query && req.query.id) {
    Client.findById(req.query.id, function (err, client) {
      if (err) {
        res.status(404);
        console.log('error while queryting the client', err)
        res.json({ error: "client doesnt exist" })
      }
      res.json(client);
    });
  } else {
    // get all Clients
    Client.find(function (err, clients) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(clients);
    });

  }
};

// expose in the root element the different entry points of the
// graphQL service
const root = {
  hello: () => 'Hello world from GraphQL!',
  getOrder: (req) => getOrder(req),
  addClient: (req) => addClient(req),
  orders: () => Object.values(orders),
  clients:() => Object.values(clients)
};

// instance the expressJS app
const app = express();
// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));

//one single endpoint different than REST
app.use('/graphql', graphqlHTTP({
  schema: graphQLschema,
  rootValue: root,
  graphiql: true,
}));

app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));