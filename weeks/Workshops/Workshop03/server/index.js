const express = require('express');
const app = express();
//Para la autenticación
const session = require('express-session');
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/todo-api");

app.use( session( {
  /* Aquí irían los atributos de nuestra sesión, como claves,
   * cómo se guarda, tiempo de expiración, etc...
   */
  domains: '*',
  methods: "*"
}));

const {
  userPatch,
  userPost,
  userGet,
  userDelete
} = require("./controllers/userController.js");

// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));


// listen to the user request

app.get("/api/users", userGet);
app.post("/api/users", userPost);
app.patch("/api/users", userPatch);
app.put("/api/users", userPatch);
app.delete("/api/users", userDelete);


app.listen(3000, () => console.log(`Example app listening on port 3000!`))
