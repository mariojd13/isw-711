const User = require("../models/userModel");

/**
 * Creates a User
 *
 * @param {*} req
 * @param {*} res
 */
const userPost = (req, res) => {
  var user = new User();

  user.username = req.body.username;
  user.password = req.body.password;
  user.firstName = req.body.firstName;
  user.lastName = req.body.lastName;

  if (user.username && user.firstName && user.lastName && user.password) {
    user.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the user', err)
        res.json({
          error: 'There was an error saving the user'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/users/?id=${user.id}`
      });
      res.json(user);
    });
  } else {
    res.status(422);
    console.log('error while saving the User')
    res.json({
      error: 'No valid data provided for User'
    });
  }
};

/**
 * Get all User
 *
 * @param {*} req
 * @param {*} res
 */
const userGet = (req, res) => {
  // if an specific User is required
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "user doesnt exist" })
      }
      res.json(user);
    });
  } else {
    // get all Users
    User.find(function (err, users) {
      if (err) {
        res.status(422);
        res.json({ "error": err });
      }
      res.json(users);
    });

  }
};

/**
 * Delete one User
 *
 * @param {*} req
 * @param {*} res
 */
const userDelete = (req, res) => {
  // if an specific User is required
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(500);
        console.log('error while queryting the user', err)
        res.json({ error: "user doesnt exist" })
      }
      //if the user exists
      if(user) {
        user.remove(function(err){
          if(err) {
            res.status(500).json({message: "There was an error deleting the user"});
          }
          res.status(204).json({});
        })
      } else {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "user doesnt exist" })
      }
    });
  } else {
    res.status(404).json({ error: "You must provide a User ID" });
  }
};

/**
 * Updates a User
 *
 * @param {*} req
 * @param {*} res
 */
const userPatch = (req, res) => {
  // get User by id
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({ error: "user doesnt exist" })
      }

      // update the user object (patch)
      user.username = req.body.username ? req.body.username : user.username;
      user.firstName = req.body.firstName? req.body.firstName : user.firstName;
      user.lastName = req.body.lastName? req.body.lastName : user.lastName;
      // update the user object (put)
      // user.title = req.body.title
      // user.detail = req.body.detail

      user.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the user', err)
          res.json({
            error: 'There was an error saving the user'
          });
        }
        res.status(200); // OK
        res.json(user);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "User doesnt exist" })
  }
};

const login = app.get('/login', function (req, res) {
  if (!req.query.username || !req.query.password) {
    res.send('login failed');
  } else if(req.query.username === "mariojd13" || req.query.password === "1234") {
    req.session.user = "mariojd13";
    req.session.admin = true;
  }
});

module.exports = {
  userGet,
  userPost,
  userPatch,
  userDelete
}